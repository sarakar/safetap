# require 'gcm'
# GCM_SENDER = GCM.new("AIzaSyDFCioSFwToIVoWGpSg1suL0eWdr00j-Fw")

require 'houston'

# Environment variables are automatically read, or can be overridden by any specified options. You can also
# conveniently use `Houston::Client.development` or `Houston::Client.production`.

APN = Houston::Client.development
APN.certificate = File.read("./development_certificate.pem")
