# config/initializers/swagger.rb

class Swagger::Docs::Config
  def self.transform_path(path, api_version)
    # Make a distinction between the APIs and API documentation paths.
    "apidocs/#{path}"
  end
end

Swagger::Docs::Config.register_apis({
  '1.0' => {
    controller_base_path: '',
    api_file_path: 'public/apidocs',
    base_path: "https://safetapapp.herokuapp.com",
    # base_path: "http://localhost:3000",
    clean_directory: true,
    camelize_model_properties: false
  }
})

GrapeSwaggerRails.options.url      = '/apidocs/api-docs.json'
GrapeSwaggerRails.options.app_url  = 'https://safetapapp.herokuapp.com'
# GrapeSwaggerRails.options.app_url  = 'http://localhost:3000'
GrapeSwaggerRails.options.app_name = 'Safetap Swagger'