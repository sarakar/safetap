Rails.application.routes.draw do

  mount GrapeSwaggerRails::Engine => '/apis_doc'
  get 'home/index'

  #active admin routes
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  #api routes
  namespace :api , :defaults => { :format => 'json' } do
    scope :module => :v1 do
      post "login_client", to: 'clients#login_client'
      post "register_client", to: 'clients#register_client'
      post 'doctor_client', to: 'clients#doctor_client'
      get 'get_all_clients', to: 'clients#get_all_clients'
      get "doctor_infos", to: 'doctors#doctor_infos'
      post "login_doctor", to: 'doctors#login_doctor'
      post 'update_profile', to: 'doctors#update_profile'
      get 'get_all_doctors', to: 'doctors#get_all_doctors'
      get 'websites', to: 'doctors#websites'
      get 'hotlines', to: 'doctors#hotlines'
      post 'forget_password', to: 'doctors#forget_password'
      post 'rating', to: 'doctors#rating'
      post 'get_credentials', to: 'doctors#get_credentials'
      post 'search_by_name', to: 'doctors#search_by_name'
      post "call_history", to: 'doctors#save_call_history'
      get "call_histories", to: 'doctors#get_call_histories'
      get "get_call_detail", to: 'doctors#get_call_detail' 
      get "get_call_histories_for_client", to: 'clients#get_call_histories_for_client' 
    end
  end

  #web
  get '/tokbox', :to => redirect('/tokbox.html')
  get '/404', :to => redirect('/404.html')
  get '/confirmation_mail' => 'doctor#confirmation_mail', :as => :confirmation_mail

  root 'home#index'
  get "sign_up", to: 'doctor#new', :as => :signup
  post "signup", to: 'doctor#create',  :as => :create
  get "doctors", to: 'doctor#index',  :as => :doctors
  #get "profile/:id(.:format)", to: 'doctor#show',  :as => :profile
  get  "login" , to: 'home#new'
  post "login" , to: 'home#create',  :as => :home
  delete "logout" , to: 'home#destroy'
  resources :doctors

end
