class CreateWebresources < ActiveRecord::Migration
  def change
    create_table :webresources do |t|
      t.string :name
      t.string :site_web

      t.timestamps null: false
    end
  end
end
