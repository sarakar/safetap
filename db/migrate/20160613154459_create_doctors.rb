class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.string :name
      t.string :last_name
      t.string :username
      t.string :password
      t.string :email
      t.string :phone_number
      t.text :picture_url
      t.text :website
      t.text :location
      t.text :about

      t.timestamps null: false
    end
  end
end
