class CreateHotlines < ActiveRecord::Migration
  def change
    create_table :hotlines do |t|
      t.string :name
      t.string :phone_number

      t.timestamps null: false
    end
  end
end
