class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.integer :doctor_id
      t.integer :client_id
      t.integer :note_value
      t.timestamps null: false
    end
  end
end
