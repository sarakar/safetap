class CreateDeviceDetails < ActiveRecord::Migration
  def change
    create_table :device_details do |t|
      t.integer :user_id
      t.integer :device_id
      t.string :device_type
      t.string :cert_type

      t.timestamps null: false
    end
  end
end