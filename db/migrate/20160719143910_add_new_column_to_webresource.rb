class AddNewColumnToWebresource < ActiveRecord::Migration
  def change
    add_column :webresources, :campus_specific, :string
  end
end
