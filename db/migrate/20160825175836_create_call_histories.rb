class CreateCallHistories < ActiveRecord::Migration
  def change
    create_table :call_histories do |t|
      t.integer :doctor_id
      t.integer :client_id
      t.text :status
      t.datetime :request_at

      t.timestamps null: false
    end
  end
end
