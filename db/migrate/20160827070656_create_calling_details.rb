class CreateCallingDetails < ActiveRecord::Migration
  def change
    create_table :calling_details do |t|
      t.string :session_id
      t.text :token
      t.integer :doctor_id
      t.integer :client_id
      t.string :status
      t.datetime :request_at

      t.timestamps null: false
    end
  end
end
