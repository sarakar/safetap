class AddNewColumnToHotline < ActiveRecord::Migration
  def change
    add_column :hotlines, :campus_specific, :string
  end
end
