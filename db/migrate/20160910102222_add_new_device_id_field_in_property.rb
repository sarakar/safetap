class AddNewDeviceIdFieldInProperty < ActiveRecord::Migration
  def change
  	add_column :device_details, :new_device_id, :string
  end
end
