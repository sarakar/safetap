class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.text :token
      t.integer :doctor_id
      t.integer :client_id
      t.text :plateform
      t.timestamps null: false
    end
  end
end
