class AddEmailVerificationFieldDoctor < ActiveRecord::Migration
  def change
  	add_column :doctors, :verification_status, :integer, default: 0
  	add_column :doctors, :verification_email_token, :string
  end
end
