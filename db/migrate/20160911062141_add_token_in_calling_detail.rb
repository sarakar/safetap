class AddTokenInCallingDetail < ActiveRecord::Migration
  def change
  	add_column :call_histories, :doctor_session_id, :string
  	add_column :call_histories, :doctor_token, :string
  	add_column :call_histories, :client_session_id, :string
  	add_column :call_histories, :client_token, :string
  	remove_column :call_histories, :request_at
  end
end
