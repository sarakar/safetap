class PushNotification

  def self.initialize_ios_app
    @ios_app ||= RailsPushNotifications::APNSApp.create(
            apns_dev_cert: File.read(Rails.root.join("development_certificate.pem")),
            apns_prod_cert: File.read(Rails.root.join("production_certificate.pem")),
            sandbox_mode: true,
        )
  end

  def self.send_to_ios(device_ids,msg,data = {})
    begin
      initialize_ios_app.notifications.create(
        destinations: device_ids,
        data: { aps: { alert: msg ,data: data, sound: "ring.wav" } }
      )
      initialize_ios_app.push_notifications
    rescue Exception => e
      return true
    end
  end

  def self.push_call_to_doctor(devices,calling_id)
    registration_ids = devices.pluck(:new_device_id)
    if registration_ids.size > 0
       send_to_ios(registration_ids,"You have a video call",{calling_id: calling_id,nType: "calling"})
    end
  end

end