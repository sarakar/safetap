class Notification

	def self.push_call_to_doctor(devices,calling_id)
		registration_ids = devices.pluck(:token)
		if registration_ids.size > 0
			registration_ids.each do |registration_id|
				push_notification(registration_id,calling_id)
			end
		end
	end

	def self.push_notification(registration_id,calling_id)
		notification = Houston::Notification.new(device: registration_id)
		notification.alert = "You have a video call"
		#notification.badge = 57
		#notification.sound = "sosumi.aiff"
		notification.category = "INVITE_CATEGORY"
		notification.content_available = true
		notification.custom_data = {calling_id: calling_id}
		APN.push(notification)
	end

end