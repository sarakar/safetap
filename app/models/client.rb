class Client < ActiveRecord::Base

	validates_format_of :username, with: /\A(?=.*[a-z])[a-z\d\s]+\Z/i
	validates :username, :presence => true, :uniqueness => true

	has_many :device
	
	def self.get_all_clients()
		clients = []
		clients= Client.all
		return {status:0, clients: clients}
	end

	def self.login_client(params)

		# return {status: 1, message:"username is required"} unless params["username"].present?
		return {status: 1, message:"phone_number is required"} unless params["phone_number"].present?

		client = Client.find_by(phone_number: params["phone_number"])

		if client
			{status:0, client: client, user_type: "client"}
		else
			{status:1, message:"client not found"}
		end
	end

	def self.register_client(params)
		return {status: 1, message:"username is required"} unless params["username"].present?
		return {status: 1, message:"phone_number is required"} unless params["phone_number"].present?

		client = Client.find_by( phone_number: params["phone_number"])

		if client
			{status:1, message: "user Already exist"}
		else
			client = Client.create(username: params["username"], phone_number: params["phone_number"])
			{status:0, client: client}
		end
	end


	def self.doctor_client(params)
		return {status: 1, message:"Phone number is required"} unless params["phone_number"].present?
		client = Client.find_by(phone_number: params["phone_number"])
		if client
			return {status:0, client:client }
		else
			return {status:1, message:"client not found"}
		end
	end

	def self.get_call_histories_for_client(params)
		return {status: 1, message:"client_id is required"} unless params["client_id"].present?
		call_history = CallHistory.where(client_id: params["client_id"])
		return {status:0, call_history: CallHistory.as_json_minimal_information(call_history)}
	end
end
