require 'carrierwave/orm/activerecord'

class Doctor < ActiveRecord::Base

	has_many :notes
	has_many :devices

	validates :username, :presence => true, :uniqueness => true
	

	mount_uploader :avatar, AvatarUploader

	def self.generate_token
		token = ""
		found = true
		while found
			token = SecureRandom.hex(10)
			found = !!Doctor.find_by(verification_email_token: token)
		end
		token
	end

	def as_json_minimal_information
	 	if self.avatar.present? && self.avatar.url.present?
		    as_json(only: [:id,:name, :last_name,:username,:password,:email,:phone_number,:website,:location,:about,:created_at]).
		    merge({ avatar: "#{Rails.application.secrets.base_url}"+self.avatar.url})
		else
		    as_json(only: [:id,:name, :last_name,:username,:password,:email,:phone_number,:website,:location,:about,:created_at]).
		    merge({ avatar: ""})
		end
	end

	def self.login_doctor(params)
		return {status: 1, message:"email is required"} unless params["email"].present?
		return {status: 1, message:"password is required"} unless params["password"].present?
		return {status: 1, message:"device_id is required"} unless params["device_id"].present?

		doctor = Doctor.find_by(email: params["email"], password: params["password"])
		if doctor
			device_detail = DeviceDetail.find_by(new_device_id: params[:device_id],user_id: doctor.id)
		    if device_detail.blank?
		      DeviceDetail.create(new_device_id: params[:device_id],user_id: doctor.id)
		    end

			{status:0, doctor: doctor.as_json_minimal_information, user_type: "doctor"}
		else
			{status:1, message:"doctor not found"}
		end
	end

	def self.doctor_infos(params)
		return {status: 1, message:"email is required"} unless params["email"].present?
		doctor = Doctor.find_by(email: params["email"])
		
		if doctor
			return {status:0, doctor: DoctorSerializer.new(doctor, root: false)}
		else
			return {status:1, message:"doctor not found"}
		end
	end

	def self.update_profile(params)
		return {status: 1, message:"doctor_id is required"} unless params["doctor_id"].present?
		return {status: 1, message:"username is required"} unless params["username"].present?
		return {status: 1, message:"name is required"} unless params["name"].present?
		return {status: 1, message:"last name is required"} unless params["last_name"].present?
		return {status: 1, message:"email is required"} unless params["email"].present?
		return {status: 1, message:"phone number is required"} unless params["phone_number"].present?
		return {status: 1, message:"website is required"} unless params["website"].present?
		return {status: 1, message:"location is required"} unless params["location"].present?
		return {status: 1, message:"about is required"} unless params["about"].present?
		return {status: 1, message:"title is required"} unless params["title"].present?

		doctor = Doctor.find_by(id: params["doctor_id"])
		doctor.avatar = params["avatar"] if params["avatar"].present?
		if doctor
			doctor_1 = Doctor.where(id:params["doctor_id"],email: params["email"])
			doctor_2 = Doctor.where.not(id:params["doctor_id"]).find_by(email: params["email"])
			if doctor_2
				return {status:1, message:"The new email is already used"}
			else
				verification_email_token = Doctor.generate_token
				doctor.update(title:params["title"], new_email:params["email"],username:params["username"],name:params["name"],last_name:params["last_name"],phone_number: params["phone_number"],website: params["website"], location: params["location"], about: params["about"],verification_email_token:verification_email_token)
				if doctor.valid? and doctor_1.blank?
					Mailer.validation_email(doctor).deliver
				end
				return {status: 1, doctor: doctor.errors.full_messages.first} if doctor.errors.any?
				return {status:0, doctor: DoctorSerializer.new(doctor, root: false)}
			end
		else
			return {status:1, message:"doctor not found"}
		end
	end

	def self.get_all_doctors()
		doctors = []
		avg_notes = []
		doctors= Doctor.all.order("name asc, last_name asc")
		doctors = ActiveModel::ArraySerializer.new(doctors, each_serializer: DoctorSerializer)
		return {status:0, doctors: doctors}
	end

	def self.websites()
		web_resources = Webresource.all
		return {status:0, list_websites: web_resources}
	end
	
	def self.hotlines()
		web_hotlines = Hotline.all
		return {status:0, list_hotlines: web_hotlines}
	end

	def self.forget_password(params)
		return {status: 1, message:"email is required"} unless params["email"].present?
		doctor = Doctor.find_by(email: params["email"])
		if doctor
			# Sends email to user when user is created.
      		Mailer.forgot_password_email(doctor).deliver
			return {status:0, message: "email has been sent"}
		else
			return {status:1, message:"doctor not found"}
		end
	end
	
	def self.rating(params)
		return {status: 1, message:"note is required"} unless params["note"].present?
		return {status: 1, message:"client id is required"} unless params["id_client"].present?
		return {status: 1, message:"doctorid is required"} unless params["id_doctor"].present?
		note = Note.find_by(doctor_id: params["id_doctor"] , client_id: params["id_client"])
		doctor = Doctor.find_by(id: params["id_doctor"])
		if params["note"].to_i > 0
			if note.present?
				note.update_attributes(note_value: params["note"])
				return {status:0, doctor: DoctorSerializer.new(doctor, root: false)}
			else
				Note.create(doctor_id: params["id_doctor"] , client_id: params["id_client"], note_value: params["note"])
				return {status:0, doctor: DoctorSerializer.new(doctor, root: false)}
			end
		else
			return {status:1, message: "note is not valid"}
		end
	end

	def  self.get_credentials(params)
		return {status: 1, message:"id_doctor is required"} unless params["id_doctor"].present?
		return {status: 1, message:"id_client is required"} unless params["id_client"].present?
		doctor = Doctor.find_by(id: params["id_doctor"])
		return {status:1, message:"Doctor not found"} if doctor.blank?
		client = Client.find_by(id: params["id_client"])
		return {status:1, message:"Client not found"} if client.blank?

		cridentienls = TokboxRest.get_cridentienls

		call_history = CallHistory.create(status:"In Process",doctor_id:params["id_doctor"],client_id:params["id_client"],doctor_session_id:cridentienls[:session_id],doctor_token:cridentienls[:doctor_token],client_session_id:cridentienls[:session_id],client_token:cridentienls[:client_token])
		puts "call_history",call_history.id
		devices = DeviceDetail.where(user_id: doctor.id)
		PushNotification.push_call_to_doctor(devices, call_history.id)
		{status: 0,session_id:cridentienls[:session_id],client_token:cridentienls[:client_token],calling_id:call_history.id}
	end

	def self.search_by_name(params)

		return {status: 1, message:"name is required"} unless params["name"].present?

		name = params["name"]
		doctors = []
		if name
			doctors= Doctor.where("name LIKE ?", "#{name}%") 
		end
		
		doctors = ActiveModel::ArraySerializer.new(doctors, each_serializer: DoctorSerializer)
		return {status:0, doctors: doctors}

	end

	def self.save_call_history(params)
		return {status: 1, message:"calling_id is required"} unless params["calling_id"].present?
		return {status: 1, message:"status is required"} unless params["status"].present?
		
		call_history = CallHistory.find_by(id: params["calling_id"])
		return {status:1, message:"call history not found"} if call_history.blank?
		if call_history.update_attributes(status: params["status"])
			{status:0, message:"Call History has been saved"}
		else
			{status:1, message: call_history.errors.full_messages.first}
		end
	end

	def self.get_call_histories(params)
		return {status: 1, message:"doctor_id is required"} unless params["doctor_id"].present?
		call_history = CallHistory.where(doctor_id: params["doctor_id"])
		return {status:0, call_history: CallHistory.as_json_minimal_information(call_history)}
	end

	def self.get_call_detail(params)
		return {status: 1, message:"calling_id is required"} unless params["calling_id"].present?
		call_history = CallHistory.find_by(id: params["calling_id"])
		return {status:1, message:"call history not found"} if call_history.blank?
		return {status:0, call_history:call_history.as_json_minimal_information}
	end

end