class CallHistory < ActiveRecord::Base

  def self.as_json_minimal_information(histories)
    call_histories =[]
    histories.each do |history|
      call_histories << history.as_json_minimal_information
    end
    call_histories
  end

  def as_json_minimal_information
    as_json(only: [:id,:status,:doctor_session_id,:doctor_token,:client_session_id,:client_token]).
    merge({ client_name: Client.find(client_id).username,
      created_at: self.created_at.strftime("%m/%d/%Y %I:%M"),
      doctor_name: Doctor.find(doctor_id).username,
    })
  end

end
