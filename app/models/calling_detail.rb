class CallingDetail < ActiveRecord::Base

  def as_json_minimal_information
    as_json(only: [:id,:session_id, :token, :created_at]).
    merge({ doctor: Doctor.find(doctor_id).as_json_minimal_information})
  end
end
