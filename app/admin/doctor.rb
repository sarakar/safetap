ActiveAdmin.register Doctor do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

	permit_params :password, :username, :title, :name, :last_name, :email, :phone_number,
					:website, :location, :about, :avatar
					
	actions :index, :show,  :update, :edit, :destroy, :new, :create

	form do |f|
	    f.inputs "Doctor Details" do
	      	f.input :name
	      	f.input :last_name
	      	f.input :username
	      	f.input :email
	      	f.input :title
	      	f.input :password
	      	f.input :phone_number
	      	f.input :website
	      	f.input :location
	      	f.input :about
	        f.input :avatar
	    end
	    f.actions
	 end


	index do
		column("Name")  do |doctor| 
			doctor.name
		end
		column("Last name")  do |doctor| 
			doctor.last_name
		end
		column("Username")  do |doctor| 
			doctor.username
		end
		column("Email") do |doctor|
			doctor.email
		end
		column("Title") do |doctor|
			doctor.title
		end
		column("Password")  do |doctor| 
			doctor.password
		end	
		column("Phone number") do |doctor|
			doctor.phone_number
		end
		column("Website") do |doctor|
			doctor.website
		end
		column("Location") do |doctor|
			doctor.location
		end
		column("About") do |doctor|
			doctor.about
		end
		column("Avatar") do |doctor|
			if doctor.avatar.file
				image_tag("/realuploads/doctor/avatar/#{doctor.id}/#{doctor.avatar.file.filename}", width:"250")
			end
		end
		actions
	end

end
