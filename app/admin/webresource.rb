ActiveAdmin.register Webresource do

	permit_params :name, :site_web, :campus_specific
					
					
	actions :index, :show,  :update, :edit, :destroy, :new, :create


	index do
		column("Name")  do |webresource| 
			webresource.name
		end

		column("Web site") do |webresource|
			webresource.site_web
		end

		column("Campus specific") do |webresource|
			webresource.campus_specific
		end

		actions
	end	

end
