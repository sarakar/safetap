ActiveAdmin.register Hotline do


	permit_params :name, :phone_number, :campus_specific
					
					
	actions :index, :show,  :update, :edit, :destroy, :new, :create


	index do

		column("Name")  do |hotline| 
			hotline.name
		end

		column("Phone number") do |hotline|
			hotline.phone_number
		end

		column("Campus specific") do |hotline|
			hotline.campus_specific
		end

		actions
	end

end