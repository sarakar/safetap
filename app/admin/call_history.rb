ActiveAdmin.register CallHistory do
	permit_params :doctor_id, :client_id,:status,:doctor_session_id,:doctor_token,:client_session_id,:client_token
	actions :index, :show,  :update, :edit, :destroy, :new, :create
end