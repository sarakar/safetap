class HomeController < ApplicationController

	def index
		
	end

	def new
		
	end

	def create
		doctor = Doctor.find_by(email: params[:session][:email])
    	if doctor.password == params[:session][:password]
    		redirect_to profile_path(id: doctor.id)
    	else
    		# Create an error message.
			render 'index'
		end
	end

	def destroy
		log_out if logged_in?
		redirect_to root_url
	end
	  
end
