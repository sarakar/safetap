class Api::V1::DoctorsController < ActionController::Base
    include Api::V1::Swagger::DoctorsSwaggerConcern
    respond_to :json

    #Done Swagger 
    def login_doctor
        result = Doctor.login_doctor(params)
        render json: result
    end

    #Done Swagger 
    def doctor_infos
        result = Doctor.doctor_infos(params)
        render json: result
    end

    #Done Swagger 
    def update_profile
    	result = Doctor.update_profile(params)
    	render json: result
    end

    #Done Swagger 
    def get_all_doctors
        result = Doctor.get_all_doctors()
        render json: result
    end

    #Done Swagger 
    def forget_password
        result = Doctor.forget_password(params)
        render json: result
    end

    #Done Swagger 
    def websites
        result = Doctor.websites()
        render json: result
    end

    #Done Swagger 
    def hotlines
        result = Doctor.hotlines()
        render json: result
    end

    #Done Swagger 
    def rating
        result = Doctor.rating(params)
        render json: result
    end

    #Done Swagger 
    def get_credentials
        result = Doctor.get_credentials(params)
        render json: result
    end

    #Done Swagger 
    def search_by_name
        result = Doctor.search_by_name(params)
        render json: result
    end

    #Done Swagger 
    def save_call_history
        result = Doctor.save_call_history(params)
        render json: result
    end
    
    #Done Swagger 
    def get_call_histories
        result = Doctor.get_call_histories(params)
        render json: result
    end

    #Done Swagger 
    def get_call_detail
        result = Doctor.get_call_detail(params)
        render json: result
    end

end