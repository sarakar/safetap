class Api::V1::ClientsController < ActionController::Base
    include Api::V1::Swagger::ClientsSwaggerConcern
    respond_to :json
    
    #Done Swagger 
	def get_all_clients
        result = Client.get_all_clients()
        render json: result
    end

    #Done Swagger 
    def login_client
        result = Client.login_client(params)
        render json: result
    end

    #Done Swagger 
    def register_client
        result = Client.register_client(params)
        render json: result
    end

    def client_infos
        result = Client.client_infos(params)
        render json: result
    end

    #Done Swagger 
    def get_call_histories_for_client
        result = Client.get_call_histories_for_client(params)
        render json: result
    end

end
