class DoctorController < ApplicationController


	def index
	    @doctors = Doctor.all
	end

	def show
	    @doctor = Doctor.find_by(email: params[:email])
	end

	def new
		@doctor = Doctor.new
	end

	def create
        @doctor = Doctor.new(doctor_params)
        @doctor.avatar = params[:avatar]
        if @doctor.save
         	flash[:success] = "Welcome to the Doctor App!"
        else
          render 'new'
    	end
    end

	def confirmation_mail
		user = Doctor.find_by(verification_email_token: params["verification_email_token"])
		if user
			user.verification_email_token = ""
			user.email = user.new_email
			user.new_email = ""
			user.save
		end
		redirect_to root_path
    end

    private
    
	def doctor_params
		params.permit(:name, :last_name, :username, :email, :phone_number, 
			:website, :location, :about, :password)
	end

end
