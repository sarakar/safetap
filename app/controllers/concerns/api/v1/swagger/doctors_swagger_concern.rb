module Api::V1::Swagger::DoctorsSwaggerConcern
  extend ActiveSupport::Concern
  included do
    swagger_controller :doctors, "Doctors Details"
  
    swagger_api :login_doctor do
      summary "/login_doctor"
      notes "Login Doctor"
      param :query, :email, :string, :optional, "email"
      param :query, :password, :string, :optional, "password"     
      param :query, :device_id, :string, :optional, "device_id"    
    end

    swagger_api :doctor_infos do
      summary "/doctor_infos"
      notes "Doctor Infos"
      param :query, :email, :string, :optional, "email"
    end

    swagger_api :update_profile do
      summary "/update_profile"
      notes "Update Profile"
      param :query, :username, :string, :optional, "Username"
      param :query, :name, :string, :optional, "Name"
      param :query, :last_name, :string, :optional, "last Name"
      param :query, :email, :string, :optional, "Email"
      param :query, :title, :string, :optional, "Title"
      param :query, :phone_number, :string, :optional, "Phone Number"
      param :query, :website, :string, :optional, "Website"
      param :query, :location, :string, :optional, "Location"
      param :query, :about, :string, :optional, "About"
      param :query, :avatar, :file, :optional, "Avatar"   
      param :query, :doctor_id, :string, :optional, "doctor_id"                                  
    end

    swagger_api :get_all_doctors do
      summary "/get_all_doctors"
      notes "Get All Doctors"
    end

    swagger_api :forget_password do
      summary "/forget_password"
      notes "Forget Password"
      param :query, :email, :string, :optional, "email"
    end

    swagger_api :websites do
      summary "/websites"
      notes "Websites"
    end

    swagger_api :hotlines do
      summary "/hotlines"
      notes "Hotlines"
    end

    swagger_api :rating do
      summary "/rating"
      notes "Rating"
      param :query, :note, :integer, :optional, "Note"
      param :query, :id_client, :string, :optional, "Id Client"
      param :query, :id_doctor, :string, :optional, "Id Doctor"
    end

    swagger_api :get_credentials do
      summary "/get_credentials"
      notes "Get Credentials"
      param :query, :id_doctor, :string, :optional, "Id Doctor"
      param :query, :id_client, :string, :optional, "Id Client"
    end

    swagger_api :search_by_name do
      summary "/search_by_name"
      notes "Search By Name"
      param :query, :name, :string, :optional, "Name"
    end

    swagger_api :save_call_history do
      summary "/save_call_history"
      notes "Save Call History"
      param :query, :calling_id, :string, :optional, "Calling Id"
      param :query, :status, :string, :optional, "Status"
    end

    swagger_api :get_call_histories do
      summary "/get_call_histories"
      notes "Get Call Histories"
      param :query, :doctor_id, :string, :optional, "Doctor Id"
    end

    swagger_api :get_call_detail do
      summary "/get_call_detail"
      notes "Get Call Detail"
      param :query, :calling_id, :string, :optional, "Calling Id"
    end

  end
end  