module Api::V1::Swagger::ClientsSwaggerConcern
  extend ActiveSupport::Concern
  included do
    swagger_controller :clients, "Clients Details"
  
    swagger_api :login_client do
      summary "/login_client"
      notes "Login client"
      param :query, :phone_number, :string, :optional, "Phone Number"
    end

    swagger_api :get_all_clients do
      summary "/get_all_clients"
      notes "Get All Clients"
    end

    swagger_api :register_client do
      summary "/register_client"
      notes "Register Client"
      param :query, :username, :string, :optional, "Username"
      param :query, :phone_number, :string, :optional, "Phone Number"
    end

    swagger_api :get_call_histories_for_client do
      summary "/get_call_histories_for_client"
      notes "Get Call Histories For Client"
      param :query, :client_id, :integer, :optional, "Client Id"
    end

  end
end  