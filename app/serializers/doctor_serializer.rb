class DoctorSerializer < ActiveModel::Serializer

	attributes :id, :name, :last_name, :title, :username, :password, :email, :phone_number,
			   :website, :location, :about, :created_at, :updated_at, :avatar, :percentage_rating,
			   :hours, :is_top, :rating



	def percentage_rating
		avg_notes = object.notes.average(:note_value) || 0
	end

	def hours
		hours = 0
	end

	def is_top
		is_top = false
		avg_notes = object.notes.average(:note_value)
		if avg_notes == 5
			is_top = true
		end
		is_top
	end

	def rating
		notes = Note.where(doctor_id: self.id)
		notes.collect(&:note_value).sum.to_f/notes.length if notes.length > 0
	end

	def avatar
		if object.avatar.present? and object.avatar.url.present?
			"#{Rails.application.secrets.base_url}"+object.avatar.url
		else
			""
		end
	end

end
