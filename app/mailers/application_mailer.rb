class ApplicationMailer < ActionMailer::Base
  default from: "safe.tap.contact@gmail.com"
  layout 'mailer'
end
