class Mailer < ApplicationMailer
	
	default from: "safe.tap.contact@gmail.com"

	def forgot_password_email(doctor)
	    @doctor = doctor
	    mail(to: @doctor.email, subject: 'Forgot password')
  	end

  	def validation_email(user)
	    @user = user
	    mail(to: @user.email, subject: 'Verification mail')
  	end
end
